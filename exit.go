// exit.go
package main

import (
	"fmt"
	"os"
)

func main() {
	defer fmt.Println("Hello World!")
	os.Exit(3)
}

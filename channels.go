package main

import "fmt"

func main() {
	messages := make(chan string)

	ackMessage := make(chan string)

	finishMessage := make(chan string)

	go func() {
		messages <- "ping"
		fmt.Println("push ping finished")
		ack := <-ackMessage
		fmt.Println(ack)
		finishMessage <- "finish"

	}()

	msg := <-messages
	fmt.Println(msg)

	ackMessage <- "ack"

	finish := <-finishMessage
	fmt.Println(finish)
}

package main

import "fmt"

func plus(a int, b int) int {
	return a + b
}

func minus(a int, b int) (c int) {
	c = a - b
	return //must have return
}

func mul(a, b int) int {
	return a * b
}

func donothing() {
	//return //option
}

func main() {
	res := plus(1, 2)
	fmt.Println("1+2 = ", res)

	res = minus(1, 2)
	fmt.Println("1-2 = ", res)

	res = mul(1, 2)
	fmt.Println("1*2 = ", res)

	donothing()
}

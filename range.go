package main

import "fmt"

func main() {
	nums := []int{2, 3, 4}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum:", sum)

	for i, num := range nums {
		if num == 3 {
			fmt.Println("index", i)
		}
	}

	kvs := map[string]string{"a": "apple", "b": "banna"}
	for k, v := range kvs {
		fmt.Printf("%s->%s\n", k, v)
	}

	text := "go翡翠"
	for i, c := range text {
		fmt.Println("unicode", i, c)
	}
	for i := 0; i < len(text); i++ {
		fmt.Println("byte", text[i])
	}

	fmt.Println(len(text), len([]rune(text)))
}

package main

import "fmt"

func main() {
	s := make([]string, 3)
	fmt.Println("emp:", s)

	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println("set:", s)
	fmt.Println("get:", s[2])
	fmt.Println("len:", len(s))

	s = append(s, "d")
	s = append(s, "e", "f")
	fmt.Println("apd:", s)

	c := make([]string, len(s))
	copy(c, s)
	fmt.Println("cpy:", c)

	l := s[2:5]
	fmt.Printf("type:%T, sl1:%v\n", l, l)

	l = s[:5]
	fmt.Println("sl2:", l)

	t := []string{"g", "h", "i"}
	t = append(t, "zzzzzzzzzzzzzzzzzz")
	fmt.Printf("type:%T, dcl:%v\n", t, t)

	tt := [3]string{"g", "h", "i"}
	//tt := append(tt, "xxxxxxxxxxxxxxxxxxxxxxx") error array can not append
	fmt.Printf("tt type:%T, dcl:%v\n", tt, tt)

	twoD := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerlen := i + 1
		twoD[i] = make([]int, innerlen)
		for j := 0; j < innerlen; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d:", twoD)
}

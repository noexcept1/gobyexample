package main

import "time"
import "fmt"

func main() {
	finishSignal := make(chan bool)

	timer1 := time.NewTimer(time.Second * 2)

	<-timer1.C
	fmt.Println("Timer 1 expired")

	timer2 := time.NewTimer(time.Second)
	go func() {
		<-timer2.C
		fmt.Println("Timer 2 expired")
		finishSignal <- true
	}()

	/*
		stop2 := timer2.Stop()
		if stop2 {
			fmt.Println("Timer 2 stopped")
		}
	*/

	if <-finishSignal {
		fmt.Println("finish all")
	}
}

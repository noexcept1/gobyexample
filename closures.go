package main

import "fmt"

func intSeq() func() int {
	i := 0
	fmt.Println("internal i:", i)
	return func() int {
		i += 1
		return i
	}
}

func main() {
	nextInt := intSeq()

	fmt.Println(nextInt())
	fmt.Println(nextInt())

	intSeq()

	fmt.Println(nextInt())

	newInt := intSeq()
	fmt.Println(newInt())
}
